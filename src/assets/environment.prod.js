const environment = {
  production: true,
  apiRootUrl: '//' + window.location.hostname.replace('//app.', '//api.') + ''
};
