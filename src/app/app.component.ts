import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LoaderService} from 'app/shared/services/loader.service';
import {ParentComponent} from 'app/shared/parent.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends ParentComponent implements OnInit {

  constructor(private translate: TranslateService,
              private loaderService: LoaderService) {
    super();
    translate.setDefaultLang('fr');
    translate.use('fr');
  }

  ngOnInit() {
    this.loaderService.hide();
  }
}
