import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  role: string;
  items: { label, path }[] = [

  ];

  manager: { label, path }[] = [
    {label: 'SIDENAV.DASHBOARD', path: ['therapist']},
    {label: 'SIDENAV.ACTIVITIES', path: ['therapist', 'activities']},
    {label: 'SIDENAV.CRITERION', path: ['therapist', 'criterion']}
  ];
  admin: { label, path }[] = [
    {label: 'SIDENAV.USER', path: ['admin', 'users']},
    {label: 'SIDENAV.LICENCE', path: ['admin', 'licence']},
    {label: 'SIDENAV.SETTING', path: ['admin', 'setting']}
  ];
  controller: { label, path }[] = [
    {label: 'SIDENAV.DASHBOARD', path: ['helper']},
    {label: 'SIDENAV.VIEWER', path: ['viewer']}
  ];

  constructor(public authService: AuthService) {
  }

  ngOnInit() {
    const userType = this.authService.getLoggedInUserType();

    if (userType) {
      this.items = this[userType];
    }
  }
}
