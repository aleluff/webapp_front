import {Component, OnInit} from '@angular/core';
import {AuthService} from 'app/shared/services/auth.service';
import {ParentComponent} from 'app/shared/parent.component';
import {UserModel} from 'app/shared/models/user.model';
import {Router} from '@angular/router';

const {version: appVersion} = require('../../../../package.json');
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends ParentComponent implements OnInit {

  loggedInUser: UserModel;
  appVersion: string;

  constructor(private authService: AuthService,
              private breadcrumbService: BreadcrumbService,
              private router: Router) {
    super();
    this.appVersion = appVersion;
  }

  logout() {
    this.breadcrumbService.reset();
    this.authService.logOut();
    this.router.navigate(['/']);
  }

  ngOnInit() {
    this.subscriptions.push(
      this.authService.loggedInUserObs.subscribe(user => this.loggedInUser = user)
    );
  }
}
