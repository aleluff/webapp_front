import {Component, OnInit} from '@angular/core';
import {ParentComponent} from 'app/shared/parent.component';
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormArray, FormControl} from '@angular/forms';
import {RequestService} from 'app/shared/services/request.service';
import {SettingService} from '../../shared/services/setting.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent extends ParentComponent implements OnInit {

  public settingForm;

  constructor(
    private activatedRoute: ActivatedRoute,
    private breadcrumbService: BreadcrumbService,
    private translateService: TranslateService,
    private fb: FormBuilder,
    private requestService: RequestService,
    private settingService: SettingService) {
    super();
  }

  async onSubmit(values) {
    values.settings.map(async function (setting) {
      if (SettingService.settingList[setting.id] !== setting.value) {
        await this.requestService.putSetting(setting);
        SettingService.settingList[setting.id] = setting.value;
      }
    }.bind(this));
  }

  async ngOnInit() {
    this.breadcrumbService.setItem(0, {label: this.translateService.instant('ADMIN.SETTING.TITLE'), link: '/admin/config'});
    await this.settingService.init();
    await this.loadSetting();
  }

  get formData() {
    return <FormArray>this.settingForm.get('settings');
  }

  async loadSetting() {
    const items = [];

    for (const [key, value] of Object.entries(SettingService.settingList)) {
      items.push(this.fb.group({
        id: new FormControl(key),
        value: new FormControl(value)
      }));
    }

    this.settingForm = this.fb.group({
      settings: this.fb.array(items)
    });
  }
}
