import {Component, OnInit} from '@angular/core';
import {ParentComponent} from 'app/shared/parent.component';
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';
import {TranslateService} from '@ngx-translate/core';
import {UserModel} from '../../shared/models/user.model';
import {MatTableDataSource} from '@angular/material/table';
import {RequestService} from '../../shared/services/request.service';
import {Router} from '@angular/router';


@Component({
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent extends ParentComponent implements OnInit {

  dataSource: MatTableDataSource<UserModel> = new MatTableDataSource();
  displayedColumns: string[] = ['first_name', 'last_name', 'email', 'role', 'disabled'];

  constructor(private breadcrumbService: BreadcrumbService,
              private translateService: TranslateService,
              private requestService: RequestService,
              private router: Router) {
    super();
  }

  ngOnInit() {
    this.breadcrumbService.setItem(0, {label: this.translateService.instant('ADMIN.USER.TITLE'), link: '/admin/user/list'});
    this.loadTable();
  }

  async loadTable() {
    this.dataSource.data = await this.requestService.getUsers();
  }

  editUser(user: UserModel) {
    const userId = user.id;
    this.router.navigate(['admin', 'users', userId]);
  }
}


