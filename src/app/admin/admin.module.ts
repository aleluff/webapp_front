import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'app/shared/shared.module';
import {RegisterComponent} from './register/register.component';
import {FormsModule} from '@angular/forms';
import {SettingComponent} from './setting/setting.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [RegisterComponent, SettingComponent],
  providers: []
})
export class AdminModule {
}


