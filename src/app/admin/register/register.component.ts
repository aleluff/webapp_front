import {Component, OnInit} from '@angular/core';
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {RequestService} from 'app/shared/services/request.service';
import {UserTypeEnum} from '../../shared/enums/user-type.enum';
import {ErrorService} from '../../shared/services/error.service';

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;

  constructor(private activatedRoute: ActivatedRoute,
              private breadcrumbService: BreadcrumbService,
              private translateService: TranslateService,
              private fb: FormBuilder,
              private errorService: ErrorService,
              private router: Router,
              private requestService: RequestService) {

    this.registerForm = fb.group({
      email: fb.control('', [Validators.email]),
      first_name: fb.control('', [Validators.required]),
      last_name: fb.control('', [Validators.required]),
      avatar: fb.control(''),
      role: fb.control(UserTypeEnum.patient, [Validators.required]),
      password: fb.control('', [Validators.required]),
      confirm_password: fb.control('', [Validators.required]),
      birth_date:fb.control('', [Validators.required]),
    });
  }

  changeListener($event: any): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = () => {
      this.registerForm.get('avatar').setValue(myReader.result);
    };
    myReader.readAsDataURL(file);
  }

  async onSubmit() {
    if (this.registerForm.value.password !== this.registerForm.value.confirm_password) {
      this.errorService.addErrorKey('USER.PASSWORD_MISMATCH');
    } else {
      this.registerForm.value.birth_date = this.registerForm.value.birth_date.getTime();
      const res = await this.requestService.postRegister(this.registerForm.value);

      if (res.id) {
        this.router.navigate(['../'], {relativeTo: this.activatedRoute});
      }
    }
  }

  ngOnInit() {
    this.breadcrumbService.setItem(0, {label: this.translateService.instant('ADMIN.USER.TITLE'), link: '/admin/users'});
    this.breadcrumbService.setItem(1, {label: this.translateService.instant('ADMIN.REGISTER.NEW'), link: '/new'});
  }
}
