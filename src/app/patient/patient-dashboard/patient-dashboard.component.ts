import {Component} from '@angular/core';
import {ParentComponent} from 'app/shared/parent.component';
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-patient-dashboard',
  templateUrl: './patient-dashboard.component.html',
  styleUrls: ['./patient-dashboard.component.scss']
})
export class PatientDashboardComponent extends ParentComponent {

  constructor(private breadcrumbService: BreadcrumbService,
              private translateService: TranslateService,
              private router: Router) {
    super();
  }
}
