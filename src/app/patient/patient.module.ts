import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'app/shared/shared.module';
import {PatientDashboardComponent} from './patient-dashboard/patient-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [PatientDashboardComponent],
  providers: []
})
export class PatientModule {
}
