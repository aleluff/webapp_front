import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {RequestService} from 'app/shared/services/request.service';
import {AuthService} from 'app/shared/services/auth.service';
import {UserService} from 'app/shared/services/user.service';
import {SettingService} from '../../shared/services/setting.service';

@Component({
  selector: 'app-auth-component',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  public authForm: FormGroup;
  public internUserManagment = SettingService.settingList[SettingService.userManagment];

  constructor(private requestService: RequestService,
              private authService: AuthService,
              private UserService: UserService,
              private fb: FormBuilder, private router: Router) {
    this.authForm = fb.group({
      email: fb.control('', [Validators.email]),
      password: fb.control('', [Validators.required])
    });

    const user = authService.getToken();
    if (user) {
      this.onLogin(user);
    }
  }

  onLogin(user) {
    const path = this.authService.getRootPath(user.role);

    this.authService.setToken(user);
    this.authService.setLoggedInUser(user);
    this.router.navigate(path);
  }

  async onSubmit(): Promise<void> {
    const email: string = this.authForm.get('email').value;
    const password: string = this.authForm.get('password').value;

    const user = await this.requestService.userAccountLogin(email, password);
    if (user) {
      this.onLogin(user);
    }
  }
}
