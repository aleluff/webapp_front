import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthComponent} from './auth/auth.component';
import {SharedModule} from 'app/shared/shared.module';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {SignupComponent} from './signup/signup.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [],
  declarations: [AuthComponent, ResetPasswordComponent, SignupComponent]
})
export class AuthModule {
}
