import {Component} from '@angular/core';
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {RequestService} from 'app/shared/services/request.service';
import {ErrorService} from '../../shared/services/error.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {

  public signupForm: FormGroup;
  public hide = true;

  constructor(private activatedRoute: ActivatedRoute,
              private breadcrumbService: BreadcrumbService,
              private errorService: ErrorService,
              private translateService: TranslateService,
              private fb: FormBuilder,
              private router: Router,
              private requestService: RequestService) {

    this.signupForm = fb.group({
      email: fb.control('', [Validators.email]),
      first_name: fb.control('', [Validators.required]),
      last_name: fb.control('', [Validators.required]),
      password: fb.control('', [Validators.required]),
      confirm_password: fb.control('', [Validators.required]),
      birth_date: fb.control('', [Validators.required]),
      role: fb.control('', [Validators.required])
    });
  }

  changeListener($event: any): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = () => {
      this.signupForm.get('avatar').setValue(myReader.result);
    };
    myReader.readAsDataURL(file);
  }

  async onSubmit() {
    if (this.signupForm.value.password !== this.signupForm.value.confirm_password) {
      this.errorService.addErrorKey('USER.PASSWORD_MISMATCH');
    } else {
      this.signupForm.value.birth_date = this.signupForm.value.birth_date.getTime();
      const res = await this.requestService.postRegister(this.signupForm.value);

      if (res.id) {
        this.router.navigate(['../'], {relativeTo: this.activatedRoute});
      }
    }
  }
}
