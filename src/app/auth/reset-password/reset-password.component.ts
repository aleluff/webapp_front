import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {RequestService} from 'app/shared/services/request.service';
import {AuthService} from 'app/shared/services/auth.service';
import {UserService} from 'app/shared/services/user.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {
  public resetForm: FormGroup;
  public msg: string;
  public submitted = false;

  constructor(private requestService: RequestService,
              private authService: AuthService,
              private UserService: UserService,
              private translateService: TranslateService,
              private fb: FormBuilder, private router: Router) {
    this.resetForm = fb.group({
      email: fb.control('', [Validators.email]),
    });
  }

  onSubmit(): void {
    const email: string = this.resetForm.get('email').value;
    this.requestService.resetPassword(email)
      .then((res) =>
        this.msg = this.translateService.instant('AUTH.PASS_RESETED'),
      )
      .then(() => this.submitted = true);
  }
}
