import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RequestService} from './services/request.service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatListModule} from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTableModule} from '@angular/material/table';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSortModule} from '@angular/material/sort';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatTreeModule} from '@angular/material/tree';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {MatNativeDateModule} from '@angular/material/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserService} from 'app/shared/services/user.service';
import {RouterLink, RouterLinkActive, RouterModule, Router} from '@angular/router';
import {ParentComponent} from 'app/shared/parent.component';
import {LoaderService} from 'app/shared/services/loader.service';
import {ErrorService} from './services/error.service';
import {ErrorComponent} from 'app/shared/error/error.component';
import {UserComponent} from 'app/shared/user/user.component';
import {DateHelperService} from './services/date-helper.service';
import {TokenInterceptor} from './interceptors/token.interceptor';
import {AuthService} from 'app/shared/services/auth.service';
import {BreadcrumbComponent} from 'app/shared/breadcrumb/breadcrumb.component';
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {UserListComponent} from '../admin/user-list/user-list.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    MatGridListModule,
    MatInputModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatCardModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    MatProgressBarModule,
    RouterModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatRadioModule,
    MatTreeModule,
    MatChipsModule,
    MatTabsModule,
    MatTooltipModule,
    MatDialogModule,
    MatStepperModule,
    MatSnackBarModule,
    MatSelectModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatSliderModule,
    MatIconModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatProgressSpinnerModule
  ],
  declarations: [ParentComponent, UserListComponent, UserComponent, ErrorComponent, BreadcrumbComponent],
  providers: [
    {
      provide: RequestService,
      deps: [HttpClient, LoaderService, Router, ErrorService]
    },
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    UserService,
    LoaderService,
    ErrorService,
    DateHelperService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AuthService,
    BreadcrumbService
  ],
  exports: [
    TranslateModule,
    MatGridListModule,
    MatInputModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatCardModule,
    MatSidenavModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatListModule,
    RouterModule,
    RouterLink,
    RouterLinkActive,
    MatRadioModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSortModule,
    MatTooltipModule,
    MatDialogModule,
    MatStepperModule,
    MatSnackBarModule,
    MatSelectModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatIconModule,
    MatTreeModule,
    MatSliderModule,
    MatProgressSpinnerModule,
    MatIconModule,
    UserListComponent,
    UserComponent,
    ErrorComponent,
    BreadcrumbComponent,
  ]
})
export class SharedModule {
}
