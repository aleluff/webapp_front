import {Injectable} from '@angular/core';
import {ReplaySubject, Observable} from 'rxjs';
import {BreadcrumbItemModel} from 'app/shared/models/breadcrumb-item.model';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable()
export class BreadcrumbService {

  private items: BreadcrumbItemModel[] = [];
  private itemsSub: ReplaySubject<BreadcrumbItemModel[]>;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.itemsSub = <ReplaySubject<BreadcrumbItemModel[]>>new ReplaySubject();
  }

  private notify(): void {
    this.itemsSub.next(this.items.slice());
  }

  get itemsObs(): Observable<BreadcrumbItemModel[]> {
    return this.itemsSub.asObservable();
  }

  reset() {
    this.items = [];
    this.notify();
  }

  setItem(level: number, item: BreadcrumbItemModel) {
    let link = item.link;
    if (level > 0) {
      link = this.items[level - 1].link.concat(link);
    }
    this.items[level] = {label: item.label, link: link};
    this.items.length = level + 1;
    this.notify();
  }
}
