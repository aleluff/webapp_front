import {Injectable} from '@angular/core';
import {UserModel} from 'app/shared/models/user.model';
import {ReplaySubject} from 'rxjs';
import {RequestService} from 'app/shared/services/request.service';

@Injectable()
export class UserService {

  private usersSub: ReplaySubject<UserModel[]>;

  constructor(private requestService: RequestService) {
    this.usersSub = <ReplaySubject<UserModel[]>>new ReplaySubject();
  }

  getUser(userAccountId: string): Promise<UserModel> {
    return this.requestService.getUser(userAccountId);
  }
}
