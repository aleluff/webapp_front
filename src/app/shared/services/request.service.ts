import {Injectable} from '@angular/core';
import {Observable, firstValueFrom} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {UserModel} from 'app/shared/models/user.model';
import {LoaderService} from 'app/shared/services/loader.service';
import {ErrorService} from 'app/shared/services/error.service';
import {Router} from '@angular/router';
import {SettingModel} from '../models/setting.model';
import {UserTypeEnum} from '../enums/user-type.enum';

function getUrl(paths: string[]) {
  // @ts-ignore environment
  return environment.apiRootUrl + '/' + paths.join('/');
}

@Injectable()
export class RequestService {

  constructor(protected httpClient: HttpClient,
              protected loaderService: LoaderService,
              protected router: Router,
              protected errorService: ErrorService) {
  }

  private manageResponse(response: Observable<any>): Promise<any> {
    this.errorService.reset();
    this.loaderService.show();
    return firstValueFrom(response)
      .then(
        res => {
          this.loaderService.hide();
          return res;
        },
        res => {
          this.loaderService.hide();
          if (res) {
            if (res.status === 401) {
              this.router.navigate(['/auth']);
              return Promise.reject(res);
            }
            if (res.error) {
              this.errorService.addError(res.error.error[0].msg);
            }
          }
          return Promise.reject(res);
        }
      );
  }

  userAccountLogin(email: string, password: string): Promise<any> {
    return this.manageResponse(
      this.httpClient.post(
        getUrl(['user', 'login']), {email, password}
      )
    ).catch(err => {
      this.errorService.addErrorKey('ERRORS.AUTH.LOGIN');
      return Promise.reject(err);
    });
  }

  getUser(userAccountId: string): Promise<UserModel> {
    return this.manageResponse(
      this.httpClient.get(
        getUrl(['user', 'id', userAccountId])
      )
    );
  }

  getAlluser(): Promise<UserModel[]> {
    return this.manageResponse(
      this.httpClient.get(
        getUrl(['user']), {params: {filter: JSON.stringify({where: {role: UserTypeEnum.helper}})}}
      )
    );
  }

  postRegister(userModel: UserModel): Promise<UserModel> {
    return this.manageResponse(
      this.httpClient.post(getUrl(['user']), userModel)
    );
  }

  putSetting(setting: SettingModel): Promise<SettingModel> {
    return this.manageResponse(
      this.httpClient.put(getUrl(['settings']), setting)
    );
  }

  updateUserInfo(userModel: UserModel): Promise<any> {
    return this.manageResponse(
      this.httpClient.patch(getUrl(['user']), userModel)
    );
  }

  resetPassword(email: string): Promise<any> {
    return this.manageResponse(
      this.httpClient.post(getUrl(['user', 'reset']), {email})
    );
  }

  getUsers(): Promise<UserModel[]> {
    return this.manageResponse(
      this.httpClient.get(getUrl(['user']))
    );
  }

  toggleActive(userId: string, disabled: boolean): Promise<UserModel> {
    return this.manageResponse(
      this.httpClient.put(getUrl(['user', 'toggle-active']), {userId, disabled})
    );
  }

  postSetting(setting: SettingModel): any {
    return firstValueFrom(this.httpClient.post(getUrl(['settings']), setting));
  }

  getSetting(): any {
    return firstValueFrom(this.httpClient.get(getUrl(['settings', 'get'])));
  }
}
