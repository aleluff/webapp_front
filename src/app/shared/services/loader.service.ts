import {Injectable} from '@angular/core';

@Injectable()
export class LoaderService {
  private loaderElt: any;
  private count: number;

  constructor() {
    this.count = 1;
    this.loaderElt = document.getElementById('loader-layer');
  }

  show() {
    this.count++;
    if (this.loaderElt && this.count <= 1) {
      this.loaderElt.style.visibility = 'visible';
    }
  }

  hide() {
    if (this.count > 0) {
      this.count--;
    }
    if (this.loaderElt && this.count === 0) {
      this.loaderElt.style.visibility = 'hidden';
    }
  }
}
