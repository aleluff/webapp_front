import {Injectable} from '@angular/core';

@Injectable()
export class DateHelperService {

  constructor() {
  }

  castToDate(stringItems: any[]): Date[] {
    for (let i = 0; i < stringItems.length; i++) {
      stringItems[i] = new Date(stringItems[i]);
    }
    return stringItems;
  }
}
