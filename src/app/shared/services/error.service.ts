import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class ErrorService {

  private errors: string[] = [];

  constructor(private translateService: TranslateService) {
  }

  get errorString(): string {
    if (!this.errors || !this.errors.length) {
      return '';
    }
    if (typeof this.errors === 'string') {
      return this.errors;
    }
    return this.errors.join('\n');
  }

  addError(error: string) {
    this.errors = this.errors || [];
    this.errors.push(error);
  }

  addErrorKey(errorKey: string) {
    this.errors = this.errors || [];
    this.errors.push(this.translateService.instant(errorKey));
  }

  reset() {
    this.errors = [];
  }

  hasErrors(): boolean {
    return this.errors && (this.errors.length > 0);
  }

}
