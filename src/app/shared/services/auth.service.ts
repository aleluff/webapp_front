import {Injectable} from '@angular/core';
import {UserModel} from 'app/shared/models/user.model';
import {ReplaySubject, Observable} from 'rxjs';
import {UserTypeEnum} from '../enums/user-type.enum';
import {SettingService} from './setting.service';

@Injectable()
export class AuthService {

  static LOCAL_STORAGE_TOKEN_KEY = 'auth_token';
  private loggedInUser: UserModel;
  private loggedInUserSub: ReplaySubject<UserModel>;

  constructor() {
    this.loggedInUserSub = <ReplaySubject<UserModel>>new ReplaySubject();
  }

  get loggedInUserObs(): Observable<UserModel> {
    return this.loggedInUserSub.asObservable();
  }

  setToken(token: Object): void {
    localStorage.setItem(AuthService.LOCAL_STORAGE_TOKEN_KEY, JSON.stringify(token));
  }

  setLoggedInUser(user: UserModel) {
    this.loggedInUser = user;
    this.notify();
  }

  getLoggedInUserType(): string {
    return this.loggedInUser && this.loggedInUser.role;
  }

  private notify(): void {
    this.loggedInUserSub.next(this.loggedInUser === null ? null : Object.assign({}, this.loggedInUser));
  }

  public getToken(): Object {
    let token = localStorage.getItem(AuthService.LOCAL_STORAGE_TOKEN_KEY);

    if (!token) {
      return null;
    }

    return JSON.parse(token);
  }

  getLoggedInUserId(): string {
    const token = this.getToken();
    if (!token) {
      return '';
    }
    return token['id'];
  }

  isLoggedIn(): boolean {
    const token = this.getToken();
    if (!token) {
      return false;
    }

    const tsCreationDate: number = new Date(token['tokenExpiration']).getTime();
    const tsNow: number = Date.now();

    return tsCreationDate > tsNow;
  }

  logOut() {
    localStorage.removeItem(AuthService.LOCAL_STORAGE_TOKEN_KEY);
    this.loggedInUser = null;
    this.notify();
  }

  getRootPath(userType: UserTypeEnum): string[] {
    switch (userType) {
      case UserTypeEnum.admin:
        return ['admin', 'users'];
      case UserTypeEnum.patient:
        return ['patient', 'dashboard'];
      case UserTypeEnum.therapist:
        return ['therapist', 'dashboard'];
      default:
        return ['helper', 'dashboard'];
    }
  }
}
