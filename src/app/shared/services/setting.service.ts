import {Injectable} from '@angular/core';
import {SettingModel} from '../models/setting.model';
import {RequestService} from './request.service';

@Injectable()
export class SettingService {

  public static downloadPdf = 'DOWNLOAD_PDF';
  public static userManagment = 'USER_MANAGMENT';
  public static jiraIssue = 'JIRA_ISSUE';
  public static therapistCanControl = 'THERAPIST_CAN_CONTROL';

  public static settingList = {
    [SettingService.downloadPdf]: true,
    [SettingService.userManagment]: true,
    [SettingService.jiraIssue]: true,
    [SettingService.therapistCanControl]: false,
  };

  constructor(private requestService: RequestService) {
  }

  async init() {
    return;
    const serverSettings = await this.requestService.getSetting();

    for (const [id, value] of Object.entries(SettingService.settingList)) {
      let existInServer = false;
      serverSettings.map(function (setting) {
        if (setting.id === id) {
          existInServer = true;
          SettingService.settingList[id] = setting.value;
        }
      });

      if (!existInServer) {
        this.requestService.postSetting(<SettingModel>{
          id: id,
          value: value
        });
      }
    }
  }
}
