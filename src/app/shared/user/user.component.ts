import {Component, OnInit} from '@angular/core';
import {ParentComponent} from 'app/shared/parent.component';
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RequestService} from 'app/shared/services/request.service';
import {AuthService} from 'app/shared/services/auth.service';
import {UserModel} from '../models/user.model';
import {ErrorService} from '../services/error.service';
import {SettingService} from '../services/setting.service';
import {UserTypeEnum} from '../enums/user-type.enum';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent extends ParentComponent implements OnInit {

  private internUserManagment = SettingService.settingList[SettingService.userManagment];
  private ownUser = true;
  private msg: string;

  public userInfoForm: FormGroup;
  public user: UserModel;
  public hide = true;
  public canEdit = this.internUserManagment;

  constructor(private breadcrumbService: BreadcrumbService,
              private translateService: TranslateService,
              private fb: FormBuilder,
              private authService: AuthService,
              private requestService: RequestService,
              private errorService: ErrorService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    super();
    this.activatedRoute.data.subscribe((data: any) => {
      this.user = data.user;
      this.ownUser = this.user.id === this.authService.getLoggedInUserId();
      this.editInfo(this.user);
    });
  }

  editInfo(res: UserModel) {
    if ((!this.internUserManagment && !this.ownUser) || this.user.role === UserTypeEnum.admin) {
      this.canEdit = true;
    }

    this.userInfoForm = this.fb.group({
      id: res.id,
      email: this.fb.control(res.email, [Validators.required]),
      first_name: this.fb.control({value: res.first_name, disabled: !this.canEdit}, [Validators.required]),
      last_name: this.fb.control({value: res.last_name, disabled: !this.canEdit}, [Validators.required]),
      password: this.fb.control({value: '', disabled: !this.canEdit}),
      confirmPassword: this.fb.control({value: '', disabled: !this.canEdit}),
      avatar: this.fb.control({value: res.avatar, disabled: !this.canEdit}, []),
      role: this.fb.control({value: res.role, disabled: true}),
      disabled: this.fb.control({value: res.disabled, disabled: !this.canEdit} || false)
    });
  }

  changeListener($event: any): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = () => {
      this.userInfoForm.get('avatar').setValue(myReader.result);
    };
    myReader.readAsDataURL(file);
  }

  async toggleActive() {
    this.msg = '';
    this.errorService.reset();

    const newValue = !this.userInfoForm.get('disabled').value;
    if (await this.requestService.toggleActive(this.user.id, newValue)) {
      this.userInfoForm.get('disabled').setValue(newValue);
      this.msg = newValue ?
        this.translateService.instant('ADMIN.USER.DISABLED') :
        this.translateService.instant('ADMIN.USER.RE_ENABLED');
    } else {
      this.msg = this.translateService.instant('ERRORS.UNKNOW');
    }
  }

  onSubmit() {
    const password: string = this.userInfoForm.get('password').value;
    const confirmPassword: string = this.userInfoForm.get('confirmPassword').value;

    this.msg = '';
    this.errorService.reset();

    if (password !== confirmPassword) {
      this.errorService.addErrorKey('USER.PASSWORD_MISMATCH');
    } else {
      delete this.userInfoForm.value.confirmPassword;
      this.requestService.updateUserInfo(this.userInfoForm.value)
        .then(() => {
            this.msg = this.translateService.instant('USER.PROFILE_UPDATED');
          }
        );
    }
  }

  ngOnInit() {
    this.breadcrumbService.setItem(0, {label: this.translateService.instant('USER.EDIT'), link: '/user'});
  }
}
