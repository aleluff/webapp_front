export enum UserTypeEnum {
  admin = 'Admin',
  therapist = 'Therapist',
  helper = 'Helper',
  patient = 'Patient'
}
