export interface SettingModel {
  id: string;
  value: boolean;
}
