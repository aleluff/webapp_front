export interface BreadcrumbItemModel {
  label: string;
  link: string;
}
