import {UserTypeEnum} from 'app/shared/enums/user-type.enum';

export interface UserModel {
  id: string;
  first_name: string;
  last_name: string;
  avatar: string;
  email: string;
  role: UserTypeEnum;
  birth_date: Date;
  token: string;
  disabled?: boolean | false;
}
