import {Component, OnInit} from '@angular/core';
import {BreadcrumbService} from 'app/shared/services/breadcrumb.service';
import {ParentComponent} from 'app/shared/parent.component';
import {BreadcrumbItemModel} from 'app/shared/models/breadcrumb-item.model';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent extends ParentComponent implements OnInit {

  items: BreadcrumbItemModel[];

  constructor(private breadcrumb: BreadcrumbService) {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(
      this.breadcrumb.itemsObs.subscribe(items => this.items = items)
    );
  }
}
