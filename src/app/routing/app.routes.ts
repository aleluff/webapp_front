import {Routes} from '@angular/router';
import {AuthComponent} from 'app/auth/auth/auth.component';
import {RegisterComponent} from 'app/admin/register/register.component';
import {SettingComponent} from '../admin/setting/setting.component';
import {UserComponent} from '../shared/user/user.component';
import {UserListComponent} from '../admin/user-list/user-list.component';
import {ResetPasswordComponent} from 'app/auth/reset-password/reset-password.component';
import {PatientDashboardComponent} from '../patient/patient-dashboard/patient-dashboard.component';
import {SignupComponent} from '../auth/signup/signup.component';
import {AuthGuard} from './guards/auth.guard';
import {UserResolve} from './resolvers/user.resolve';
import {PatientResolve} from './resolvers/patient.resolve';
import {AdminResolve} from './resolvers/admin.resolve';

export const appRoutes: Routes = [
  {path: 'auth', component: AuthComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'resetpassword', component: ResetPasswordComponent},
  {
    path: 'user', component: UserComponent, resolve: {user: UserResolve}, canActivate: [AuthGuard]
  },
  {
    path: 'patient', resolve: {user: PatientResolve}, canActivate: [AuthGuard], children: [
      {path: 'dashboard', pathMatch: 'full', component: PatientDashboardComponent}
    ]
  },
  {
    path: 'admin', resolve: {admin: AdminResolve}, canActivate: [AuthGuard], children: [
      {
        path: 'users', children: [
          {
            path: '',
            pathMatch: 'full',
            component: UserListComponent
          },
          {path: 'new', component: RegisterComponent},
          {path: ':userId', resolve: {user: UserResolve}, component: UserComponent}
        ]
      },
      {path: 'setting', component: SettingComponent},
    ]
  },
  {path: '**', redirectTo: '/auth'}
];
