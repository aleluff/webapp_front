import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AuthGuard} from 'app/routing/guards/auth.guard';
import {PatientResolve} from 'app/routing/resolvers/patient.resolve';
import {AdminResolve} from './resolvers/admin.resolve';
import {UserResolve} from './resolvers/user.resolve';
import {appRoutes} from './app.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  providers: [
    AuthGuard, PatientResolve, AdminResolve, UserResolve
  ],
  exports: [RouterModule]
})
export class RoutingModule {
}
