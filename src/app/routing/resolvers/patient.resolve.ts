import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AuthService} from 'app/shared/services/auth.service';
import {UserService} from '../../shared/services/user.service';

@Injectable()
export class PatientResolve implements Resolve<Promise<any>> {

  constructor(private UserService: UserService,
              private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
    return this.UserService.getUser(this.authService.getLoggedInUserId())
      .then(user => this.authService.setLoggedInUser(user));
  }
}
