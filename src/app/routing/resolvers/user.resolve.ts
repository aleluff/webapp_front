import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AuthService} from 'app/shared/services/auth.service';
import {UserModel} from '../../shared/models/user.model';
import {UserService} from '../../shared/services/user.service';

@Injectable()
export class UserResolve implements Resolve<Promise<any>> {

  constructor(private UserService: UserService,
              private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<UserModel> {
    this.UserService.getUser(this.authService.getLoggedInUserId())
      .then(user => this.authService.setLoggedInUser(user));

    return this.UserService.getUser(route.params['userId'] || this.authService.getLoggedInUserId());
  }
}
