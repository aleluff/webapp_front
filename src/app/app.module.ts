import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AuthModule} from 'app/auth/auth.module';
import {SharedModule} from 'app/shared/shared.module';
import {CoreModule} from 'app/core/core.module';
import localeFr from '@angular/common/locales/fr';
import {registerLocaleData} from '@angular/common';
import {AdminModule} from './admin/admin.module';
import {SettingService} from './shared/services/setting.service';
import {PatientModule} from './patient/patient.module';
import {RoutingModule} from './routing/routing.module';

registerLocaleData(localeFr, 'fr');

export function settingServiceFactory(settings: SettingService) {
  return () => settings.init();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, AuthModule, SharedModule, CoreModule, AdminModule, PatientModule, RoutingModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'fr'
    },
    SettingService,
    {
      provide: APP_INITIALIZER,
      useFactory: settingServiceFactory,
      deps: [SettingService],
      multi: true
    }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
