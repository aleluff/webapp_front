# Step 1: Build the app in image 'builder'
FROM node:latest AS builder

ENV NODE_ENV=production
RUN curl -f https://get.pnpm.io/v6.16.js | node - add --global pnpm

WORKDIR /usr/src/app

COPY package.json pnpm-lock.yaml ./

RUN pnpm install --frozen-lockfile --prod

COPY . .

RUN pnpm run prod

RUN rm dist/assets/environment.prod.js

# Step 2: Use build output from 'builder'
FROM nginx:latest

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html
COPY --from=builder /usr/src/app/dist/ .

EXPOSE 80
